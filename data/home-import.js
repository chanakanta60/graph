var data_home_import = [{
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2015,
        "baht": 89747038992
    },
    {
        "product": "พืชอาหาร",
        "year": 2015,
        "baht": 69921432610
    },
    {
        "product": "กากและเศษที่เหลือใช้ทำอาหารสัตว์",
        "year": 2015,
        "baht": 63602931324
    },
    {
        "product": "พืชน้ำมัน",
        "year": 2015,
        "baht": 41320938990
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2015,
        "baht": 33518533593
    },
    {
        "product": "ผลิตภัณฑ์อาหารอื่นๆ",
        "year": 2015,
        "baht": 21293771104
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2016,
        "baht": 113317103152
    },
    {
        "product": "พืชอาหาร",
        "year": 2016,
        "baht": 64217397952
    },
    {
        "product": "กากและเศษที่เหลือใช้ทำอาหารสัตว์",
        "year": 2016,
        "baht": 59880938192
    },
    {
        "product": "พืชน้ำมัน",
        "year": 2016,
        "baht": 47185174134
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2016,
        "baht": 38157768905
    },
    {
        "product": "ผลิตภัณฑ์อาหารอื่นๆ",
        "year": 2016,
        "baht": 24058131649
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2017,
        "baht": 125150757822
    },
    {
        "product": "พืชอาหาร",
        "year": 2017,
        "baht": 51937971658
    },
    {
        "product": "กากและเศษที่เหลือใช้ทำอาหารสัตว์",
        "year": 2017,
        "baht": 62908316049
    },
    {
        "product": "พืชน้ำมัน",
        "year": 2017,
        "baht": 43903706573
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2017,
        "baht": 36774347807
    },
    {
        "product": "ผลิตภัณฑ์อาหารอื่นๆ",
        "year": 2017,
        "baht": 24496515601
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2018,
        "baht": 131621085028
    },
    {
        "product": "พืชอาหาร",
        "year": 2018,
        "baht": 53646484692
    },
    {
        "product": "กากและเศษที่เหลือใช้ทำอาหารสัตว์",
        "year": 2018,
        "baht": 71918747846
    },
    {
        "product": "พืชน้ำมัน",
        "year": 2018,
        "baht": 41017100979
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2018,
        "baht": 33416909063
    },
    {
        "product": "ผลิตภัณฑ์อาหารอื่นๆ",
        "year": 2018,
        "baht": 26234094239
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2019,
        "baht": 120207715188
    },
    {
        "product": "พืชอาหาร",
        "year": 2019,
        "baht": 60753797954
    },
    {
        "product": "กากและเศษที่เหลือใช้ทำอาหารสัตว์",
        "year": 2019,
        "baht": 62566967954
    },
    {
        "product": "พืชน้ำมัน",
        "year": 2019,
        "baht": 43258142018
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2019,
        "baht": 34030981925
    },
    {
        "product": "ผลิตภัณฑ์อาหารอื่นๆ",
        "year": 2019,
        "baht": 26224170576
    }
]
var data_home_export = [{
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2015,
        "baht": 200306835497
    },
    {
        "product": "ข้าวและผลิตภัณฑ์",
        "year": 2015,
        "baht": 172780984575
    },
    {
        "product": "ผลิตภัณท์จากสัตว์",
        "year": 2015,
        "baht": 123311009739
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2015,
        "baht": 106541107492
    },
    {
        "product": "น้ำตาลและผลิตภัณฑ์",
        "year": 2015,
        "baht": 98560697204
    },
    {
        "product": "มันสำปะหลังและผลิตภัณฑ์",
        "year": 2015,
        "baht": 93982844968
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2016,
        "baht": 212322890097
    },
    {
        "product": "ข้าวและผลิตภัณฑ์",
        "year": 2016,
        "baht": 172554579497
    },
    {
        "product": "ผลิตภัณท์จากสัตว์",
        "year": 2016,
        "baht": 129771767433
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2016,
        "baht": 126365259856
    },
    {
        "product": "น้ำตาลและผลิตภัณฑ์",
        "year": 2016,
        "baht": 95410361662
    },
    {
        "product": "มันสำปะหลังและผลิตภัณฑ์",
        "year": 2016,
        "baht": 80057643242
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2017,
        "baht": 211138629575
    },
    {
        "product": "ข้าวและผลิตภัณฑ์",
        "year": 2017,
        "baht": 193758558534
    },
    {
        "product": "ผลิตภัณท์จากสัตว์",
        "year": 2017,
        "baht": 142987825468
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2017,
        "baht": 137677544833
    },
    {
        "product": "น้ำตาลและผลิตภัณฑ์",
        "year": 2017,
        "baht": 101984775531
    },
    {
        "product": "มันสำปะหลังและผลิตภัณฑ์",
        "year": 2017,
        "baht": 72723249656
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2018,
        "baht": 203752170740
    },
    {
        "product": "ข้าวและผลิตภัณฑ์",
        "year": 2018,
        "baht": 201236969965
    },
    {
        "product": "ผลิตภัณท์จากสัตว์",
        "year": 2018,
        "baht": 151337941741
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2018,
        "baht": 106313332161
    },
    {
        "product": "น้ำตาลและผลิตภัณฑ์",
        "year": 2018,
        "baht": 103562374721
    },
    {
        "product": "มันสำปะหลังและผลิตภัณฑ์",
        "year": 2018,
        "baht": 74086900013
    },
    {
        "product": "ผลิตภัณฑ์จากสัตว์น้ำ",
        "year": 2019,
        "baht": 188668830293
    },
    {
        "product": "ข้าวและผลิตภัณฑ์",
        "year": 2019,
        "baht": 150875573274
    },
    {
        "product": "ผลิตภัณท์จากสัตว์",
        "year": 2019,
        "baht": 152773365804
    },
    {
        "product": "ผลไม้และผลิตภัณฑ์",
        "year": 2019,
        "baht": 102185828411
    },
    {
        "product": "น้ำตาลและผลิตภัณฑ์",
        "year": 2019,
        "baht": 105079418358
    },
    {
        "product": "มันสำปะหลังและผลิตภัณฑ์",
        "year": 2019,
        "baht": 55924782992
    }
]

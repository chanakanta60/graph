var data_dealer = [{
        "continent": "เอเชีย",
        "year": 2015,
        "export": 624782,
        "import": 115710
    },
    {
        "continent": "แอฟิกา",
        "year": 2015,
        "export": 54928,
        "import": 6482
    },
    {
        "continent": "ยุโรป",
        "year": 2015,
        "export": 126097,
        "import": 71400
    },
    {
        "continent": "อเมิกาเหนือ",
        "year": 2015,
        "export": 141867,
        "import": 75063
    },
    {
        "continent": "อเมริกาใต้",
        "year": 2015,
        "export": 2912,
        "import": 2472
    },
    {
        "continent": "ออสเตเรีย",
        "year": 2015,
        "export": 35197,
        "import": 44650
    },
    {
        "continent": "เอเชีย",
        "year": 2016,
        "export": 605749,
        "import": 135993
    },
    {
        "continent": "แอฟิกา",
        "year": 2016,
        "export": 48801,
        "import": 5490
    },
    {
        "continent": "ยุโรป",
        "year": 2016,
        "export": 126495,
        "import": 74998
    },
    {
        "continent": "อเมิกาเหนือ",
        "year": 2016,
        "export": 155765,
        "import": 69876
    },
    {
        "continent": "อเมริกาใต้",
        "year": 2016,
        "export": 2942,
        "import": 1942
    },
    {
        "continent": "ออสเตเรีย",
        "year": 2016,
        "export": 37191,
        "import": 44247
    },
    {
        "continent": "เอเชีย",
        "year": 2017,
        "export": 674272,
        "import": 138330
    },
    {
        "continent": "แอฟิกา",
        "year": 2017,
        "export": 57410,
        "import": 5162
    },
    {
        "continent": "ยุโรป",
        "year": 2017,
        "export": 129280,
        "import": 63653
    },
    {
        "continent": "อเมิกาเหนือ",
        "year": 2017,
        "export": 158968,
        "import": 70100
    },
    {
        "continent": "อเมริกาใต้",
        "year": 2017,
        "export": 3758,
        "import": 1957
    },
    {
        "continent": "ออสเตเรีย",
        "year": 2017,
        "export": 37628,
        "import": 49167
    },
    {
        "continent": "เอเชีย",
        "year": 2018,
        "export": 626065,
        "import": 141969
    },
    {
        "continent": "แอฟิกา",
        "year": 2018,
        "export": 56898,
        "import": 4690
    },
    {
        "continent": "ยุโรป",
        "year": 2018,
        "export": 128879,
        "import": 61459
    },
    {
        "continent": "อเมิกาเหนือ",
        "year": 2018,
        "export": 150005,
        "import": 81117
    },
    {
        "continent": "อเมริกาใต้",
        "year": 2018,
        "export": 3904,
        "import": 2103
    },
    {
        "continent": "ออสเตเรีย",
        "year": 2018,
        "export": 39093,
        "import": 45345
    },
    {
        "continent": "เอเชีย",
        "year": 2019,
        "export": 578784,
        "import": 148131
    },
    {
        "continent": "แอฟิกา",
        "year": 2019,
        "export": 45847,
        "import": 3671
    },
    {
        "continent": "ยุโรป",
        "year": 2019,
        "export": 118044,
        "import": 64947
    },
    {
        "continent": "อเมิกาเหนือ",
        "year": 2019,
        "export": 146688,
        "import": 72403
    },
    {
        "continent": "อเมริกาใต้",
        "year": 2019,
        "export": 4515,
        "import": 2888
    },
    {
        "continent": "ออสเตเรีย",
        "year": 2019,
        "export": 36012,
        "import": 45920
    }
]
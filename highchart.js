function basiccolumn(divid, title, data, category, name) {

    Highcharts.chart(divid, {
        colors: ['#FFA500', '#20B2AA', '#B22222'],
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },

        xAxis: {
            categories: category,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: name
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} ล้านบาท</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: data,
        credits: {
            enabled: false
        },
    });
}


function barchart(divid, title, data, category, name) {
    Highcharts.chart(divid, {
        colors: ['#FFA500', '#20B2AA', '#B22222', '#F0E68C', '#808000'],
        chart: {
            type: 'column',

        },
        title: {
            text: title
        },

        xAxis: {
            categories: category
        },
        yAxis: {
            title: {
                text: name
            },
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                pointWidth: 45,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:,.0f} ล้านบาท'
                },
                cursor: 'pointer'
            },

        },

        series: [{
            name: name,
            type: 'column',
            colorByPoint: true,
            data: data,
            showInLegend: false
        }],
        credits: {
            enabled: false
        },

    });
}

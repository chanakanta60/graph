Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'ผลิตภัณฑ์จากสัตว์น้ำ',
            y: 19.27,

        }, {
            name: 'ข้าวและผลิตภัณฑ์',
            y: 15.01
        }, {
            name: 'ผลิตภัณท์จากสัตว์',
            y: 13.65
        }, {
            name: 'ผลไม้และผลิตภัณฑ์',
            y: 8.87
        }, {
            name: 'น้ำตาลและผลิตภัณฑ์',
            y: 7.19
        }, {
            name: 'dddddddddddddddddddddd',
            y: 4.57
        },{
            name: 'มันสำปะหลังและผลิตภัณฑ์',
            y: 4.57
        }, {
            name: 'Opera',
            y: 1.6
        }]
    }]
});

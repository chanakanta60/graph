function circlechart(divid, title, data, category, name) {
    Highcharts.chart(divid, {
        //colors: ['#FFA500', '#20B2AA', '#B22222', '#F0E68C', '#808000'],
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name:data[0],
                y:category[0],
            },{
                name:data[1],
                y:category[1],
            },{
                name:data[2],
                y:category[2],
            },{
                name:data[3],
                y:category[3],
            },{
                name:data[4],
                y:category[4],
            },{
                name:data[5],
                y:category[5],
            }]
        }]
    });
}
